* Build LAMP Stack Docker image using Jenkins Pipeline and push to the Docker Hub

```
pipeline {
    environment {
        registry = "surangajayalath/lamp" 
        registryCredential = 'dockerhub_id'
        dockerImage = ''
}
agent any
    stages {
        stage('Cloning our Git') {
            steps {
                git branch: 'main', credentialsId: 'dockerhub_id', url: 'https://gitlab.com/surangajayalath/test-php-jenkins.git'
            }
        }
    stage('Building our image') {
        steps{
            script {
                dockerImage = docker.build registry + ":$BUILD_NUMBER"
            }
        }
    }
    stage('Deploy our image') {
        steps{
            script {
                docker.withRegistry('',registryCredential ) {
                dockerImage.push()
                }
            }   
        }
    }
    stage('Cleaning up') {
        steps{
            sh "docker rmi $registry:$BUILD_NUMBER"
        }
    }
}
}
```
**## Careful with following steps**
```
environment {
        registry = "surangajayalath/lamp" 
        registryCredential = 'dockerhub_id'

        dockerImage = ''
}
```
* registry = "<docker_hub_username>/simple name"
* registryCredential = <first_add_docker_hub_credential_to_the_jenkins_and_get_the_id_and_paste_here>

```
stage('Cloning our Git') {
            steps {
                git branch: 'main', credentialsId: 'dockerhub_id', url: 'https://gitlab.com/surangajayalath/test-php-jenkins.git'
            }
        }
```

* Go through "Pipeline Syntax" and generate pipelin code and add here
* sudo chmod 666 /var/run/docker.sock


